#!/usr/bin/python

import os
import sys
import argparse
from natsort import natsorted

parser = argparse.ArgumentParser(description='Concatenate files')
parser.add_argument("--dry", help="only show list of files to process", action="store_true")
parser.add_argument("--init", help="set initial file in concatenating sequence")
parser.add_argument('path', help='directory for process')
parser.add_argument('extension', help='extension of files')

try:
    options = parser.parse_args()
except:
    parser.print_help()
    sys.exit(0)


ordered_files = []

for root, dirs, files in os.walk(options.path):
    # files.sort()
    files = natsorted(files)
    for file in files:
        if os.path.splitext(file)[1] == options.extension:
            filePath = os.path.join(root, file)
            ordered_files.append(filePath)


if options.init:
    index = 0
    for file in ordered_files:
        if os.path.samefile(file, options.init):
            break
        index += 1
    ordered_files.insert(0, ordered_files.pop(index))


print(ordered_files)
if options.dry:
    sys.exit(0)

with open("output" + options.extension, "ab") as output_file:
    for file in ordered_files:
        with open(file, "rb") as input_file:
            output_file.write(input_file.read())
